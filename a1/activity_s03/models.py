from django.db import models

# Create your models here.

class GroceryItem(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    status = models.CharField(max_length=50,  default = 'pending')
    date_created = models.DateField("date created", auto_now=True)
