from django.apps import AppConfig


class ActivityS03Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'activity_s03'
