from django.urls import path
from . import views

urlpatterns = [
    path('list', views.groceries, name="grocerylist"),
    path('<int:grocery_item_id>',  views.grocery_item, name="groceryitem"),
    path('register', views.register, name="register"),
    path('change_password', views.change_password, name="changepassword"),
    path('login',  views.user_login, name="login"),
    path('logout', views.user_logout, name="logout")
    
]
