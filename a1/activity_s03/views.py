from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import GroceryItem
from django.template import loader

from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def groceries(req):
    grocery_list = GroceryItem.objects.all()
    context = {
        'grocery_list': grocery_list,
        'user': req.user
    }
    return render(req, 'grocery_list.html', context)

from django.forms.models import model_to_dict
def grocery_item(req, grocery_item_id):
    groceryitem = model_to_dict(GroceryItem.objects.get(pk = grocery_item_id))
    return render(req, 'grocery_item.html', groceryitem)

def register(req):
    users = User.objects.all()
    is_user_registered = False
    
    user = User()
    user.username = 'johndoe'
    user.first_name = 'John'
    user.last_name = 'Doe'
    user.email = 'johndoe@gmail.com'
    user.set_password('123')
    user.is_staff = False
    user.is_active= True
    
    for registered_user in users:
        if registered_user.username == user.username:
            is_user_registered = True
            break
    
    if is_user_registered == False:
        user.save()
        
    context  = {
        'first_name': user.first_name,
        'last_name': user.last_name,
        'is_user_registered': is_user_registered
    }
    
    return render(req, 'register.html', context)

def change_password(req):
    is_user_authed = False
    
    username = 'johndoe'
    password = '123'
    
    user = authenticate(username = username,  password = password)
    
    if user is not None:
        authed_user = User.objects.get(username = username)
        authed_user.set_password('1234')
        authed_user.save()
        is_user_authed = True
        
    context = {
        'is_user_authed': is_user_authed
    }
    
    return render(req, 'change_password.html', context)
        
def user_login(req):
    username = "johndoe"
    password = "1234"
    
    user = authenticate(username = username, password = password) 
    
    if user is not None:
        login(req, user)
        return redirect("grocerylist")
    else: 
        return render(req, "login.html")

def user_logout(req): 
    logout(req)
    return redirect('grocerylist')